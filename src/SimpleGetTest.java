
//Program for rest assured api

import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
public class SimpleGetTest 
{
 @Test
  public void GetUserDetails()
  {
	 RestAssured.baseURI = "https://reqres.in";
	 
	 RequestSpecification httpRequest = RestAssured.given();
	 
	 
	Response response = httpRequest.request(Method.GET,"/api/users?page=2");
	 
	 String responseBody = response.getBody().asString();
	 System.out.println("Response Body is :" +responseBody);
  }
}
